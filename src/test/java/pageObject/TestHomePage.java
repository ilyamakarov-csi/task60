package pageObject;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import pages.pageObject.HomePage;
import pages.pageObject.LoginPage;

/**
 * Created by IlyaMakarov on 12/21/2016.
 */
public class TestHomePage {

    private WebDriver driver;
    private final static String LOGIN = "IlyaMakarov";
    private final static String PASSWORD = "Password1";

    @BeforeClass
    public void setUp() {
        driver = new FirefoxDriver();
    }

    @AfterClass
    public void tearDown() {
        //driver.quit();
    }

    @Test
    public void logoutTest() {
        LoginPage loginPage = new LoginPage(driver);
        HomePage homePage = loginPage.login(LOGIN, PASSWORD);
        loginPage = homePage.signOut();
        Assert.assertTrue(loginPage.isSubmitButtonDisplayed(), "Submit button is not displayed");
    }
}
