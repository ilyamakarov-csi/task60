package pageFactory;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import pages.factoryObject.HomePage;
import pages.factoryObject.LoginPage;

/**
 * Created by IlyaMakarov on 12/21/2016.
 */
public class TestHomePage {

    private WebDriver driver;
    private final static String LOGIN = "IlyaMakarov";
    private final static String PASSWORD = "Password1";

    @BeforeClass
    public void setUp() {
        driver = new FirefoxDriver();
    }

    @AfterClass
    public void tearDown() {
        //driver.quit();
    }

    @Test
    public void logoutTest() {
        LoginPage loginPage = PageFactory.initElements(driver, LoginPage.class);
        HomePage homePage = loginPage.login(LOGIN, PASSWORD);
        loginPage = homePage.signOut();
        Assert.assertTrue(loginPage.isSubmitButtonDisplayed());
    }
}
