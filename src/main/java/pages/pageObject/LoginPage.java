package pages.pageObject;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

/**
 * Created by IlyaMakarov on 12/21/2016.
 */
public class LoginPage {

    private WebDriver driver;
    private final static String ADDRESS = "https://192.168.100.26/";
    private final static By USERNAME_INPUT = By.id("Username");
    private final static By PASSWORD_INPUT = By.id("Password");
    private final static By SUBMIT_BUTTON = By.id("SubmitButton");

    public LoginPage(WebDriver driver) {
        this.driver = driver;
        driver.get(ADDRESS);
    }

    public HomePage login(String login, String password) {
        driver.findElement(USERNAME_INPUT).sendKeys(login);
        driver.findElement(PASSWORD_INPUT).sendKeys(password);
        driver.findElement(SUBMIT_BUTTON).click();

        return new HomePage(driver);
    }

    public boolean isSubmitButtonDisplayed() {
        return driver.findElement(SUBMIT_BUTTON).isDisplayed();
    }
}
